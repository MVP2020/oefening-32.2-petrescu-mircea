﻿using System.ComponentModel;

namespace _32._2
{
    class ToDo : INotifyPropertyChanged, IDataErrorInfo
    {
        private string _wanneer;
        private string _wat;
        private string _wie;

        private void RaisePropertyString(string info)
        {

        }
        public ToDo(string wie, string wat, string wanneer)
        {
            Wanneer = wanneer;
            Wie = wie;
            Wat = wat;
        }

        public string Error { get; }
        //public string This [string columnName] {


        public event PropertyChangedEventHandler PropertyChanged;

        public string Wanneer
        {
            get { return _wanneer; }

            set
            {
                _wanneer = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Wanneer"));
                }
            }
        }





        public string Wat
        {
            get { return _wat; }
            set
            {
                _wat = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Wat"));
                }
            }
        }
        public string Wie
        {
            get { return _wie; }
            set
            {
                _wie = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Wie"));
                }
            }
        }

        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    default: return null;


                    case "Wie":
                        if (string.IsNullOrWhiteSpace(Wie))
                        {
                            return "You need to type a name for the activity!";
                        }
                        else
                        {
                            return null;
                        }
                    case "Wanneer":
                        if (string.IsNullOrWhiteSpace(Wanneer))
                        {
                            return "You need to type a time for the activity!";
                        }
                        else
                        {
                            return null;
                        }
                    case "Wat":
                        if (string.IsNullOrWhiteSpace(Wat))
                        {
                            return "You need to type a description for the activity!";
                        }
                        else
                        {
                            return null;
                        }
                }

            }

        }

        public override string ToString()
        {
            return Wie + " " + Wat + " " + Wanneer;
        }






    }
}
