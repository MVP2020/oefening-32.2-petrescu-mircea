﻿using System.Collections.ObjectModel;
using System.Windows;

namespace _32._2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private ObservableCollection<ToDo> ListActivities = new ObservableCollection<ToDo>();
        public MainWindow()
        {
            InitializeComponent();



            ListActivities.Add(new ToDo("Mircea", "Cycle", "06-07-2020"));
            ListActivities.Add(new ToDo("Radu", "Cook", "07-07-2020"));
            ListActivities.Add(new ToDo("Fons", "Fix Car", "09-07-2020"));

            cmbToDo.ItemsSource = ListActivities;
        }



    }






}

